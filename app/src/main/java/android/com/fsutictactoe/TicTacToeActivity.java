package android.com.fsutictactoe;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.Toast;

import java.util.Random;

/**
 * Michael Danko
 *
 * main tic-tac-toe game
 */


public class TicTacToeActivity extends Activity {

    private enum gamePiece {
        X(0), O(1);
        private final int index;

        gamePiece(int index) {
            this.index = index;
        }

        public int value() {
            return index;
        }
    }

    private int totalHumanPlayers = 0,   // Total human players, passed from previous activity selection
            row = 0, column = 0,  // find location on board
            humanGamePiece = -1, computerGamePiece = -1; // Intialize both game pieces to blank.

    private int turnCount=1; // Keep count of total turns

    // Gameboard keeps track of which spaces are taken by whom, -1 if blank.
    private int gameBoard[][] = new int[][]{{-1, -1, -1},
            {-1, -1, -1},
            {-1, -1, -1}};

    // Setup class definition of buttons for use throughout various methods.
    ImageButton button1;
    ImageButton button2;
    ImageButton button3;
    ImageButton button4;
    ImageButton button5;
    ImageButton button6;
    ImageButton button7;
    ImageButton button8;
    ImageButton button9;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // reset variables
        turnCount = 1;
        // Eliminate titlebar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tictactoe);

        // Pull humanPlayer variable from previous activity
        Intent previousIntent = getIntent();
        totalHumanPlayers = previousIntent.getIntExtra("Players", 0);

        // ResetGameBoard
        ResetGameBoard();

        if (totalHumanPlayers == 1) {
            determineStartingPlayer();
        }

        button1 = (ImageButton) findViewById(R.id.button1);
        button2 = (ImageButton) findViewById(R.id.button2);
        button3 = (ImageButton) findViewById(R.id.button3);
        button4 = (ImageButton) findViewById(R.id.button4);
        button5 = (ImageButton) findViewById(R.id.button5);
        button6 = (ImageButton) findViewById(R.id.button6);
        button7 = (ImageButton) findViewById(R.id.button7);
        button8 = (ImageButton) findViewById(R.id.button8);
        button9 = (ImageButton) findViewById(R.id.button9);

        final ImageButton imageButtonArray[] = {button1, button2, button3,
                button4, button5, button6,
                button7, button8, button9};

        for (int i = 0; i < imageButtonArray.length; i++) {
            final int j = i;
            imageButtonArray[j].setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    row = j / 3;
                    column = j % 3;

                    // Set game board depending on current player
                    if (currentPlayer() == gamePiece.X.value()) {
                        gameBoard[row][column] = gamePiece.X.value();

                    } else if (currentPlayer() == gamePiece.O.value()) {
                        gameBoard[row][column] = gamePiece.O.value();
                    }
                    // do not allow additional selections of this tile
                    imageButtonArray[j].setClickable(false);
                    // increment turn
                    ++turnCount;
                    // check if currentplayer has three in a row
                    CheckWinner();
                    // allow computer to go, if 2 human players selected returns immediately
                    // ComputerTurn passes to another method which contains a very primitive AI
                    // used to block 2 consecutive human tiles, later version can contain more
                    // advanced AI depending on development time. My initial plan was to implement
                    // various stages of the AI algorithm into different methods but time does not
                    // permit at this time.
                    ComputerTurn();
                    // check again if player 2/computer has three in a row
                    CheckWinner();
                    // redraw game board to screen. AKA set image buttons to correct images.
                    RedrawGameBoard();
                }
            });
        }

    }

    // resets class defined buttons to the correct images based on gameBoard Array.
    public void RedrawGameBoard() {
        final ImageButton imageButtonArray[] = {button1, button2, button3,
                button4, button5, button6,
                button7, button8, button9};

        for (int row = 0; row <= 2; ++row) {
            for (int col = 0; col <= 2; ++col) {
                if (gameBoard[row][col] == gamePiece.X.value()) {
                    imageButtonArray[(row * 3) + col].setImageResource(R.drawable.x);
                } else if (gameBoard[row][col] == gamePiece.O.value()) {
                    imageButtonArray[(row * 3) + col].setImageResource(R.drawable.o);
                }
            }
        }
    }

    // Between matches resets the gameBoard to blank
    public void ResetGameBoard() {
        for (int row = 0; row <= 2; ++row) {
            for (int col = 0; col <= 2; ++col) {
                gameBoard[row][col] = -1;
            }
        }
    }

    // Checks how many human players, if only 1 starts to run through AI algorithm. At this point
    // AI only attempts to block 2 in a row, will win if it has 2 in a row, otherwise a random tile
    // is chosen
    public void ComputerTurn() {
        if (totalHumanPlayers == 2) {
            return;
        } else if (totalHumanPlayers == 1) {
            ComputerWin();
        }
    }

    // Algorithm for the computer to win if it has two consecutive tiles
    // This function contains three parts of AI algorithm, Win if 2 tiles in row, block if 2
    // opponent tiles in a row, and then pick a random tile if none of the other conditions exist.
    // This method could be broken up into 3 different methods however time does not permit at this
    // time. All functionality is working.
    public void ComputerWin() {
        ++turnCount;

        if (turnCount > 9) {
            return;
        }
        //Check rows, all parts are a brute force check of the entire gameboard, recursive functions
        // may be possible however the algorithm has not been developed by me to do so. Small game
        // board prevents this from being a huge issue but a more efficient algorithm is always
        // preferred.
        for (int row = 0; row <= 2; row++) {
            if ((gameBoard[row][0] == gameBoard[row][1])
                    && (gameBoard[row][2] == -1)
                    && (gameBoard[row][0] == computerGamePiece)) {

                gameBoard[row][2] = computerGamePiece;
                return;

            } else if ((gameBoard[row][1] == gameBoard[row][2])
                    && (gameBoard[row][0] == -1)
                    && (gameBoard[row][1] == computerGamePiece)) {

                gameBoard[row][0] = computerGamePiece;
                return;

            } else if ((gameBoard[row][0] == gameBoard[row][2])
                    && (gameBoard[row][1] == -1)
                    && (gameBoard[row][0] == computerGamePiece)) {

                gameBoard[row][1] = computerGamePiece;
                return;

            }
        }

        // Check Columns
        for (int col = 0; col <= 2; col++) {
            if ((gameBoard[0][col] == gameBoard[1][col])
                    && (gameBoard[2][col] == -1)
                    && (gameBoard[0][col] == computerGamePiece)) {

                gameBoard[2][col] = computerGamePiece;
                return;

            } else if ((gameBoard[1][col] == gameBoard[2][col])
                    && (gameBoard[0][col] == -1)
                    && (gameBoard[1][col] == computerGamePiece)) {

                gameBoard[0][col] = computerGamePiece;
                return;

            } else if ((gameBoard[0][col] == gameBoard[2][col])
                    && (gameBoard[1][col] == -1)
                    && (gameBoard[0][col] == computerGamePiece)) {

                gameBoard[1][col] = computerGamePiece;
                return;

            }
        }

        // Check Diagonals
        if ((gameBoard[0][0] == gameBoard[1][1])
                && (gameBoard[2][2] == -1)
                && (gameBoard[1][1] == computerGamePiece)) {

            gameBoard[2][2] = computerGamePiece;
            return;

        } else if ((gameBoard[0][0] == gameBoard[2][2])
                && (gameBoard[1][1] == -1)
                && (gameBoard[0][0] == computerGamePiece)) {

            gameBoard[1][1] = computerGamePiece;
            return;

        } else if ((gameBoard[1][1] == gameBoard[2][2])
                && (gameBoard[0][0] == -1)
                && (gameBoard[1][1] == computerGamePiece)) {

            gameBoard[0][0] = computerGamePiece;
            return;

        } else if ((gameBoard[0][2] == gameBoard[2][0])
                && (gameBoard[1][1] == -1)
                && (gameBoard[0][2] == computerGamePiece)) {

            gameBoard[1][1] = computerGamePiece;
            return;

        } else if ((gameBoard[1][1] == gameBoard[0][2])
                && (gameBoard[2][0] == -1)
                && (gameBoard[1][1] == computerGamePiece)) {

            gameBoard[2][0] = computerGamePiece;
            return;

        } else if ((gameBoard[1][1] == gameBoard[2][0])
                && (gameBoard[0][2] == -1)
                && (gameBoard[1][1] == computerGamePiece)) {

            gameBoard[0][2] = computerGamePiece;
            return;

        }

        // ------ Blocking Code -------- //
        // ---- Blocks User Wins ------- //
        //Check rows
        for (int row = 0; row <= 2; row++) {
            if ((gameBoard[row][0] == gameBoard[row][1])
                    && (gameBoard[row][2] == -1)
                    && (gameBoard[row][0] == humanGamePiece)) {

                gameBoard[row][2] = computerGamePiece;
                return;

            } else if ((gameBoard[row][1] == gameBoard[row][2])
                    && (gameBoard[row][0] == -1)
                    && (gameBoard[row][1] == humanGamePiece)) {

                gameBoard[row][0] = computerGamePiece;
                return;

            } else if ((gameBoard[row][0] == gameBoard[row][2])
                    && (gameBoard[row][1] == -1)
                    && (gameBoard[row][0] == humanGamePiece)) {

                gameBoard[row][1] = computerGamePiece;
                return;

            }
        }

        // Check Columns
        for (int col = 0; col <= 2; col++) {
            if ((gameBoard[0][col] == gameBoard[1][col])
                    && (gameBoard[2][col] == -1)
                    && (gameBoard[0][col] == humanGamePiece)) {

                gameBoard[2][col] = computerGamePiece;
                return;

            } else if ((gameBoard[1][col] == gameBoard[2][col])
                    && (gameBoard[0][col] == -1)
                    && (gameBoard[1][col] == humanGamePiece)) {

                gameBoard[0][col] = computerGamePiece;
                return;

            } else if ((gameBoard[0][col] == gameBoard[2][col])
                    && (gameBoard[1][col] == -1)
                    && (gameBoard[0][col] == humanGamePiece)) {

                gameBoard[1][col] = computerGamePiece;
                return;

            }
        }

        // Check Diagonals
        if ((gameBoard[0][0] == gameBoard[1][1])
                && (gameBoard[2][2] == -1)
                && (gameBoard[1][1] == humanGamePiece)) {

            gameBoard[2][2] = computerGamePiece;
            return;

        } else if ((gameBoard[0][0] == gameBoard[2][2])
                && (gameBoard[1][1] == -1)
                && (gameBoard[0][0] == humanGamePiece)) {

            gameBoard[1][1] = computerGamePiece;
            return;

        } else if ((gameBoard[1][1] == gameBoard[2][2])
                && (gameBoard[0][0] == -1)
                && (gameBoard[1][1] == humanGamePiece)) {

            gameBoard[0][0] = computerGamePiece;
            return;

        } else if ((gameBoard[0][2] == gameBoard[2][0])
                && (gameBoard[1][1] == -1)
                && (gameBoard[0][2] == humanGamePiece)) {

            gameBoard[1][1] = computerGamePiece;
            return;

        } else if ((gameBoard[1][1] == gameBoard[0][2])
                && (gameBoard[2][0] == -1)
                && (gameBoard[1][1] == humanGamePiece)) {

            gameBoard[2][0] = computerGamePiece;
            return;

        } else if ((gameBoard[1][1] == gameBoard[2][0])
                && (gameBoard[0][2] == -1)
                && (gameBoard[1][1] == humanGamePiece)) {

            gameBoard[0][2] = computerGamePiece;
            return;

        }

        // No win or block oppurtunities, choose a random tile
        int row = 0, col = 0;
        do {
            Random r = new Random();
            row = r.nextInt(3);
            col = r.nextInt(3);
            Log.v("Randoms", "R:" + row + " C:" + col);
        } while (gameBoard[row][col] != -1);
        gameBoard[row][col] = computerGamePiece;
    }

    // function checks if three identical gamePieces are in three tiles in a row/column/diagonal
    public void CheckWinner() {
        // Check Rows
        for (int row = 0; row <= 2; row++) {
            if ((gameBoard[row][0] == -1) || (gameBoard[row][1] == -1) || (gameBoard[row][2] == -1)) {
                // Do Nothing
            } else if ((gameBoard[row][0] == gameBoard[row][1]) && (gameBoard[row][1] == gameBoard[row][2])) {
                ShowWinner(gameBoard[row][0]);
                return;
            }
        }

        // Check Columns
        for (int col = 0; col <= 2; col++) {
            if ((gameBoard[0][col] == -1) || (gameBoard[1][col] == -1) || (gameBoard[2][col] == -1)) {
                // Do Nothing
                Log.v("Columns", "Col:" + col);
            } else if ((gameBoard[0][col] == gameBoard[1][col]) && (gameBoard[1][col] == gameBoard[2][col])) {
                ShowWinner(gameBoard[0][col]);
                return;
            }
        }

        // Check Diagonals
        if (gameBoard[1][1] == -1) {
            // Do Nothing
        } else if ((gameBoard[0][0] == gameBoard[1][1]) && (gameBoard[1][1] == gameBoard[2][2])) {
            ShowWinner(gameBoard[1][1]);
        } else if ((gameBoard[0][2] == gameBoard[1][1]) && (gameBoard[1][1] == gameBoard[2][0])) {
            ShowWinner(gameBoard[1][1]);
        }

        // If the board is filled up and no one has 3 in a row will display the stalemate message
        if (turnCount > 9) {
            ShowWinner(-1);
        }

    }

    // Alert Dialog to tell the ending status of the game, X wins, O wins, or No Winner
    public void ShowWinner(int winner) {
        AlertDialog winnerDialog = new AlertDialog.Builder(this)
                .setTitle("Winner!")
                .setMessage("Somebody Wins! Play Again?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        TicTacToeActivity.this.finish();
                        Intent playerSelectionIntent = new Intent(TicTacToeActivity.this, PlayerSelection.class);
                        startActivity(playerSelectionIntent);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        TicTacToeActivity.this.finish();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert).create();

        if (winner == gamePiece.X.value()) {
            winnerDialog.setTitle("Winner!");
            winnerDialog.setMessage("X Wins! Play Again?");
        } else if (winner == gamePiece.O.value()) {
            winnerDialog.setTitle("Winner!");
            winnerDialog.setMessage("O Wins! Play Again?");
        } else {
            winnerDialog.setTitle("No Winner");
            winnerDialog.setMessage("No Winners. Play Again?");
        }
        winnerDialog.show();
    }

    // Who goes first, human or computer? (1 player only) Additional implementation required for
    // human player to be 'O'. Also can be used later if computer V computer is implemented.
    public void determineStartingPlayer() {

        humanGamePiece = gamePiece.X.value();
        computerGamePiece = gamePiece.O.value();
    }

    // Determine whose turn is next
    public int currentPlayer() {
        if ((turnCount % 2) == 1) {
            return gamePiece.X.value(); // X goes next
        } else {
            return gamePiece.O.value(); // O goes next
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.welcome_screen, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}

