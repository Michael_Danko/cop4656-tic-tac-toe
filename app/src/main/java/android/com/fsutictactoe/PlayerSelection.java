package android.com.fsutictactoe;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;

public class PlayerSelection extends Activity {

    private int humanPlayers = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player_selection);

        View.OnClickListener buttonListener = new View.OnClickListener() {
            // determine the amount of players
            public void onClick (View v) {
                switch (v.getId()){
                    case R.id.PlayerVPlayer: humanPlayers = 2;
                        break;
                    case R.id.PlayerVComputer: humanPlayers = 1;
                        break;
                }
                // On selection, start the next activity, the actual game, and pass in the amount
                // of human players.
                finish();
                Intent ticTacToeIntent = new Intent(PlayerSelection.this, TicTacToeActivity.class);
                ticTacToeIntent.putExtra("Players", humanPlayers);
                startActivity(ticTacToeIntent);
            }
        };
        findViewById(R.id.PlayerVPlayer).setOnClickListener(buttonListener);
        findViewById(R.id.PlayerVComputer).setOnClickListener(buttonListener);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.player_selection, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
